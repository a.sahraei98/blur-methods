# Blur Methods


Here I am sharing a comparison between four blur methods including Average, Gaussian, Median, and Bilateral. I used kernel size 7 for all methods. According to the result, the Median method outperformed the others
