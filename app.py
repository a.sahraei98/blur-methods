import cv2 as cv
import numpy as np

lena = cv.imread("images/salt_Lena.jpg")


# Average Blur
average = cv.blur(lena, (7, 7))
# cv.imshow("Average Blur", average)

# Gaussian Blur
gaussian = cv.GaussianBlur(lena, (7, 7), 0)
# cv.imshow("Gaussian Blur", gaussian)

# Median Blur
median = cv.medianBlur(lena, 7)

# Bilateral Blur
bilateral = cv.bilateralFilter(lena, 7, 75, 25)

result = np.concatenate((lena, average, gaussian), axis=1)
result2 = np.concatenate((median, bilateral), axis=1)

cv.imshow("Salt noise, Average, Guassian", result)
cv.imshow("Median, Bilateral", result2)
cv.waitKey(0)
cv.destroyAllWindows()
